import { combineReducers, createStore, applyMiddleware, compose } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import ReduxThunk from "redux-thunk";

// reducers
import user from "./user";

const rootReducer = combineReducers({
  user
});

const persistConfig = {
  timeout: 0,
  key: "1a",
  storage,
  blacklist: ["user"]
  // whitelist: ["user"]
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  persistedReducer,
  {},
  composeEnhancers(applyMiddleware(ReduxThunk))
);
// store.subscribe(() => {
//   console.log(store.getState());
// });
const persistor = persistStore(store);

export { store, persistor };
