// action types
const SET_FCM_TOKEN = "boilerplate2019/user/SET_USER_TOKEN";
const SET_CREDENTIALS = "boilerplate2019/user/SET_CREDENTIALS";

const SIGN_IN_ATTEMPT = "boilerplate2019/user/SIGN_IN_ATTEMPT";
const SIGN_IN_SUCCESS = "boilerplate2019/user/SIGN_IN_SUCCESS";
const SIGN_IN_FAILURE = "boilerplate2019/user/SIGN_IN_FAILURE";

// initial state
const INITIAL_STATE = {
  isRequesting: false,
  error: "",
  authToken: "i have authorization!",
  fcmToken: "",
  name: "Vasiliy",
  surname: "Ovechkin"
};

// reducer
export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case SET_FCM_TOKEN:
      return { ...state, fcmToken: payload };
    case SET_CREDENTIALS:
      return { ...state, [payload.type]: payload.value };
    case SIGN_IN_ATTEMPT:
      return { ...state, isRequesting: true };
    case SIGN_IN_SUCCESS:
      return { ...state, isRequesting: false, authToken: payload };
    case SIGN_IN_FAILURE:
      return { ...state, isRequesting: false, error: payload };
    default:
      return state;
  }
};

// actions
export const setFcmToken = payload => ({ type: SET_FCM_TOKEN, payload });
export const setCredentials = payload => ({ type: SET_CREDENTIALS, payload });
