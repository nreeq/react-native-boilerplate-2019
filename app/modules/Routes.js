const home = "/home";
const signIn = "/sign-in";

export const routes = {
  default: "/",
  home,
  signIn
};
