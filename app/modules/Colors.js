export const colors = {
  primary: "#11172B", // ink
  primaryLighter: "#1D2236", // ink darker

  secondary: "#25A8FC", // blue
  secondaryDarker: "#0099FC", // blue darker

  tertiary: "#FFDF37", // yellow
  tertiaryDarker: "#ECC600", // yellow darker

  black: "#000000", // black
  white: "#ffffff", // white

  faded: "#F2F2F2", // gray
  fadedDarker: "#E6E6E6" // gray darker
};
