export const fonts = {
  // montserrat
  PrimarySemiBold: "Montserrat-SemiBold",
  PrimaryRegular: "Montserrat-Regular",
  PrimaryMedium: "Montserrat-Medium",
  PrimaryBold: "Montserrat-Bold",

  // noto sans
  SecondaryRegular: "NotoSans-Regular",
  SecondaryItalic: "NotoSans-Italic",
  SecondaryBold: "NotoSans-Bold"
};
