import { connect } from "react-redux";
import { compose, lifecycle } from "recompose";
import { withRouter } from "react-router-native";
import { routes } from "./../modules";

// selectors
import { getFcmToken, getCredentials, getAuthToken } from "./../selectors";

// actions
import { setFcmToken, setCredentials } from "./../ducks/user";

export const withFcmToken = compose(
  withRouter,
  connect(
    state => ({
      fcmToken: getFcmToken(state)
    }),
    { setFcmToken }
  )
);

export const withCredentials = compose(
  withRouter,
  connect(
    state => getCredentials(state),
    { setCredentials }
  )
);

export const withAuthToken = connect(state => ({
  authToken: getAuthToken(state)
}));

export const withAuthorization = compose(
  withRouter,
  withAuthToken,
  lifecycle({
    componentDidMount() {
      const { authToken, history } = this.props;
      // maybe some deep rules for this, depends on case you use.
      if (authToken) {
        history.replace(routes.home);
      } else {
        history.replace(routes.singIn);
      }
    }
  })
);
