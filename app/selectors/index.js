import { createSelector } from "reselect";

export const getFcmToken = createSelector(
  [state => state.user.fcmToken],
  fcmToken => fcmToken
);

export const getCredentials = createSelector(
  [state => state.user.name, state => state.user.surname],
  (name, surname) => ({ name, surname })
);

export const getAuthToken = createSelector(
  [state => state.user.authToken],
  authToken => authToken
);
