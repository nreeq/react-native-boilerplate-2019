import styled from "styled-components";
import { colors, fonts } from "../modules";

const StyledText = styled.Text`
  color: ${({ color }) => color || colors.black};
  text-align: ${({ textAlign }) => textAlign || "left"};
  height: ${({ height }) => (height ? height + "px" : "auto")};
  padding: ${({ padding }) => padding || 0};
`;

export const H1Text = styled(StyledText)`
  font-family: ${({ fontFamily }) =>
    fontFamily ? fontFamily : fonts.PrimarySemiBold};
  font-size: 24px;
  line-height: 36px;
`;
export const H2Text = styled(StyledText)`
  font-family: ${({ fontFamily }) =>
    fontFamily ? fontFamily : fonts.PrimarySemiBold};
  font-size: 18px;
  line-height: 27px;
`;
export const H3Text = styled(StyledText)`
  font-family: ${({ fontFamily }) =>
    fontFamily ? fontFamily : fonts.PrimarySemiBold};
  font-size: 14px;
  line-height: 20px;
`;
