import React from "react";
import styled from "styled-components";
import { rgba } from "../helpers";
import { colors, fonts } from "./../modules";

export const Button = ({
  text = "",
  variant = "v1",
  onPress = () => {},
  iconRight,
  iconLeft,
  ...props
}) => (
  <ButtonWrapper
    onPress={onPress}
    variant={variant}
    activeOpacity={0.8}
    {...props}
  >
    {iconLeft && <IconLeft source={iconLeft} />}
    <ButtonText>{text}</ButtonText>
    {iconRight && <IconRight source={iconRight} />}
  </ButtonWrapper>
);

const variants = {
  v1: {
    bgColor: colors.secondaryDarker
  },
  v2: {
    bgColor: rgba(colors.primary, 0.25)
  }
};

const IconLeft = styled.Image`
  margin-right: 8px;
`;
const IconRight = styled.Image`
  margin-left: 8px;
`;

const ButtonWrapper = styled.TouchableOpacity`
  padding: 10px 20px;
  background-color: ${({ variant }) => variants[variant].bgColor};
  border-radius: 28px;
  align-items: center;
  justify-content: center;
  flex-direction: row;
`;

const ButtonText = styled.Text`
  text-transform: uppercase;
  color: #fff;
  font-size: 18px;
  font-family: ${fonts.PrimaryMedium};
`;
