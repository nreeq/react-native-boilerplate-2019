import React from "react";
import styled from "styled-components";
import { colors } from "../modules";
import { MyProfile } from "./../containers/MyProfile";

export const HomeScreen = () => (
  <Container>
    <MyProfile />
  </Container>
);

const Container = styled.View`
  flex: 1;
  background-color: ${colors.primaryLighter};
`;
