import i18n from "i18n-js";
import en from "./locales/en.json";
import ua from "./locales/ua.json";

i18n.fallbacks = true;
i18n.defaultLocale = "ua";

// i18n.locale = "ua";
i18n.locale = "en";

i18n.translations = {
  en,
  ua
};

export const t = i18n.t.bind(i18n);
