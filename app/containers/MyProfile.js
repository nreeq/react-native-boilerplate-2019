import React from "react";
import styled from "styled-components";
import { colors } from "../modules";
import { H2Text } from "./../components";
import { withCredentials } from "../hoc";
import { t } from "../i18n";

export const MyProfile = withCredentials(({ name, surname }) => (
  <Container>
    <H2Text color={colors.white}>{t("hello", { name, surname })}</H2Text>
  </Container>
));

const Container = styled.View`
  flex: 1;
  padding: 24px;
`;
