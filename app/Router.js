import React from "react";
import { NativeRouter as Router, Switch, Route } from "react-router-native";
import { HomeScreen } from "./screens";
import styled from "styled-components";
import { routes } from "./modules";
import { H1Text } from "./components";
import { withAuthorization } from "./hoc";
import { withRouter } from "react-router-native";

const DummyPage = withRouter(({ location }) => {
  console.log("WHERE AM I ??? location:", location.pathname);
  return <H1Text>Just a dummy image</H1Text>;
});

const Navigator = withAuthorization(() => null);

export default () => (
  <Router>
    <Container>
      <Navigator />
      <Switch>
        <Route
          exact
          path={routes.default}
          component={() => (
            <SplashScreen
              source={{
                url:
                  "http://papers.co/wallpaper/papers.co-vz04-digital-art-color-rainbow-pattern-background-34-iphone6-plus-wallpaper.jpg"
              }}
            />
          )}
        />
        <Route exact path={routes.home} component={HomeScreen} />
        <Route path="*" component={DummyPage} />
      </Switch>
    </Container>
  </Router>
);

const Container = styled.View`
  flex: 1;
`;
const SplashScreen = styled.Image`
  height: 100%;
  width: 100%;
`;
