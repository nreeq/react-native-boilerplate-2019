export const fetchReduxHelper = ({
  url,
  dispatch,
  options = {},
  innerKey = "",
  parseAction,
  attempt,
  success,
  error,
  key
}) => {
  if (attempt)
    dispatch({
      type: attempt
    });

  return fetch(url, options)
    .then(response => {
      if (!response.ok) {
        throw Error(response.statusText || `Request failed for ${url}`);
      }
      return response.json();
    })
    .then(data => {
      if (parseAction) return parseAction(data);
      const raw = data[innerKey] || data;
      const payload = {
        allIds: [],
        byId: {}
      };
      raw.forEach(it => {
        payload.allIds.push(it[key]);
        payload.byId[it[key]] = it;
      });
      return payload;
    })
    .then(payload => dispatch({ type: success, payload }))
    .catch(err => {
      dispatch({ type: error, payload: err });
      console.log(err);
    });
};

export const rgba = (hex, alpha) => {
  if (!hex || !alpha) return;

  const r = parseInt(hex.slice(1, 3), 16);
  const g = parseInt(hex.slice(3, 5), 16);
  const b = parseInt(hex.slice(5, 7), 16);

  return `rgba(${r}, ${g}, ${b}, ${alpha})`;
};
